﻿using System;
using System.Collections.Generic;
using System.Text;
using static CitizenFX.Core.Native.API;

namespace cip_database_test.Util
{
    public struct MetricsInfo
    {
        public int count;
        public double total;
        public double min;
        public double max;
        public bool failed;
        public string failedReason;
    }

    public class Metrics
    {
        private static Dictionary<string, MetricsInfo> metricsEntries = new Dictionary<string, MetricsInfo>();

        public static void AddQueryPerformance(string name, double time)
        {
            MetricsInfo perfStats = GetInfo(name);
            perfStats.count++;
            perfStats.total += time;

            if (time < perfStats.min) perfStats.min = time;
            if (time > perfStats.max) perfStats.max = time;

            metricsEntries[name] = perfStats;
        }

        public static void SetFailed(string name, string message)
        {
            MetricsInfo perfStats = GetInfo(name);
            perfStats.failed = true;
            perfStats.failedReason = message;

            metricsEntries[name] = perfStats;
        }

        public static void Dump(bool print = true)
        {
            StringBuilder fileContents = new StringBuilder();

            foreach (var metric in metricsEntries)
            {
                double avg = metric.Value.total / metric.Value.count;

                string message = $"Name: {metric.Key} | Count: {metric.Value.count} " +
                    $"| Time(avg): {avg}ms | Time(min): {metric.Value.min}ms | Time(max): {metric.Value.max}ms " +
                    $"| Failed: {metric.Value.failed} | Failed Reason: {metric.Value.failedReason}";

                if (print)
                {
                    CipDebug.Print(message, LogType.Warning);
                }

                fileContents.AppendLine(message);
            }

            string filename = "db-test-" + DateTime.Now.ToFileTime().ToString();
            SaveResourceFile("cip-database-test", filename, fileContents.ToString(), fileContents.Length);
        }

        private static MetricsInfo GetInfo(string name)
        {
            MetricsInfo metricsInfo;

            if (!metricsEntries.ContainsKey(name))
            {
                metricsInfo = new MetricsInfo
                {
                    count = 0,
                    total = 0.0,
                    min = double.MaxValue,
                    max = double.MinValue,
                    failed = false,
                    failedReason = "unknown",
                };
            }
            else
            {
                metricsEntries.TryGetValue(name, out metricsInfo);
            }

            return metricsInfo;
        }
    }
}
