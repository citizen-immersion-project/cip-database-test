﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace cip_database_test.Util
{
    public enum DatabaseCommandType
    {
        Unknown,
        DDL, // Data definition language.
        DML, // Data modification language.
        DCL, // Data control language.
        TCL, // Transaction control language.
    }

    public class Database
    {
        private dynamic exports;

        public Database(dynamic exports)
        {
            this.exports = exports;
        }

        public void ExecuteNonQuery(string name, string query, IDictionary<string, object> parameters = null)
        {
            parameters = parameters ?? new Dictionary<string, object>();
            CheckCommandType(query, parameters);

            Stopwatch sw = new Stopwatch();

            sw.Start();

            try
            {
                exports.ExecuteNonQuery(query, parameters);
            }
            catch(Exception e)
            {
                Metrics.SetFailed(name, e.Message);
            }

            sw.Stop();

            Metrics.AddQueryPerformance(name, sw.Elapsed.TotalMilliseconds);
        }

        public int ExecuteNonQueryResult(string name, string query, IDictionary<string, object> parameters = null)
        {
            int result = -1;

            parameters = parameters ?? new Dictionary<string, object>();
            CheckCommandType(query, parameters);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                result = exports.ExecuteNonQueryResult(query, parameters);

                if(result == -1)
                {
                    Metrics.SetFailed(name, "Failed to execute query, result returned -1.");
                }
            }
            catch (Exception e)
            {
                Metrics.SetFailed(name, e.Message);
            }

            sw.Stop();

            Metrics.AddQueryPerformance(name, sw.Elapsed.TotalMilliseconds);

            return result;
        }

        private static DatabaseCommandType CheckCommandType(string query, IDictionary<string, object> parameters)
        {
            string[] DDLCommands = new string[]{ "CREATE", "DROP", "ALTER", "TRUNCATE", "COMMENT", "RENAME" };
            string[] DMLCommands = new string[] { "SELECT", "INSERT", "UPDATE", "DELETE", "MERGE", "CALL", "EXPLAIN_PLAN", "LOCK TABLE" };
            string[] DCLCommands = new string[] { "GRANT", "REVOKE" };
            string[] TCLCommands = new string[] { "COMMIT", "ROLLBACK", "SAVEPOINT", "SET TRANSACTION"};

            bool isDDL = DDLCommands.Any(query.Contains);
            bool isDML = DMLCommands.Any(query.Contains);
            bool isDCL = DCLCommands.Any(query.Contains);
            bool isTCL = TCLCommands.Any(query.Contains);

            if (isDDL)
            { 
                CipDebug.Assert(parameters == null || parameters.Count == 0, "DDL commands cannot contain any parameters");
                CipDebug.Assert(!query.Contains("@"), "DDL command contains @, indicating it's relying on parameters. Which cannot be used for DDL command.");

                return DatabaseCommandType.DDL;
            }
            else if(isDML)
            {
                //Add DML asserts!
                return DatabaseCommandType.DML;
            }
            else if(isDCL)
            {
                //Add DCL asserts!
                return DatabaseCommandType.DCL;
            }
            else if(isTCL)
            {
                //Add TCL asserts
                return DatabaseCommandType.TCL;
            }

            return DatabaseCommandType.Unknown;
        }

    }

}
