﻿using CitizenFX.Core;

namespace cip_database_test.Util
{
    public enum LogType
    {
        Default,
        Functionality,
        Performance,
        Warning,
        Error
    }

    public static class CipDebug
    {
        public static void Print(string message, LogType type)
        {
            switch (type)
            {
                case LogType.Default:
                    Debug.WriteLine($"^6[CIP-DATABASE-TEST] {message}^7");
                    break;
                case LogType.Functionality:
                    Debug.WriteLine($"^5[CIP-DATABASE-TEST] {message}^7");
                    break;
                case LogType.Performance:
                    Debug.WriteLine($"^4[CIP-DATABASE-TEST] {message}^7");
                    break;
                case LogType.Warning:
                    Debug.WriteLine($"^3[CIP-DATABASE-TEST] {message}^7");
                    break;
                case LogType.Error:
                    Debug.WriteLine($"^1[CIP-DATABASE-TEST] {message}^7");
                    break;
            }
        }

        public static void Assert(bool statement, string message)
        {
            if(!statement)
            {
                Print("Assetion failed: " + message, LogType.Error);
            }
        }

    }
}
