﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using CitizenFX.Core;

using cip_database_test.Util;
using System.Threading.Tasks;

namespace cip_database_test
{
    public class CipDatabaseTestServer : BaseScript
    {
        public dynamic DbExports;

        public CipDatabaseTestServer()
        {
            DbExports = Exports["cip-database"];

            Tick += OnTick;
        }

        private async Task OnTick()
        {
            await Delay(5000);

            RunTests(20);

            Tick -= OnTick;
        }

        private async void RunTests(int repeats)
        {
            CipDebug.Print("Testing database resource started", LogType.Default);

            Database db = new Database(DbExports);

            #region Initializing
            Repeat(repeats, () => db.ExecuteNonQuery("select1", "SELECT 1", null));
            await Delay(1);
            #endregion

            #region Database
            Repeat(repeats, (i) => db.ExecuteNonQuery("create_db", "CREATE DATABASE name" + i.ToString(), null));
            await Delay(1);

            Repeat(repeats, (i) => db.ExecuteNonQuery("delete_db", "DROP DATABASE name" + i.ToString(), null));
            await Delay(1);

            Repeat(repeats, (i) => db.ExecuteNonQueryResult("create_db_result", "CREATE DATABASE name_result" + i.ToString(), null));
            await Delay(1);

            Repeat(repeats, (i) => db.ExecuteNonQueryResult("delete_db_result", "DROP DATABASE name_result" + i.ToString(), null));
            await Delay(1);

            #endregion
            //CREATE TABLE `test`.`test` ( `id` INT NOT NULL AUTO_INCREMENT, `test` VARCHAR NOT NULL , `test2` BOOLEAN NOT NULL , `test3` FLOAT NOT NULL , PRIMARY KEY(`id`)) ENGINE = InnoDB;


            Metrics.Dump();
            CipDebug.Print("Testing has been completed!", LogType.Default);
        }

        private void Repeat(int count, Action a)
        {
            for (int i = 0; i < count; i++)
            {
                a.Invoke();
            }
        }

        private void Repeat(int count, Action<int> a)
        {
            for (int i = 0; i < count; i++)
            {
                a.Invoke(i);
            }
        }
    }
    
}
